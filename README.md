Technology stack: JS, Webpack, HTML5, CSS3, Handlebars, SCSS.
To run the sample, follow these steps:
1. Install NodeJs from official site
2. Clone or Fork project
3. In console: npm install
4. In console: npm install cross-env
5. In console: npm run start